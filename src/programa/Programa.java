package programa;

import java.util.Scanner;
import clases.GestorCliente;

public class Programa {

	/**
	 * @author Daniel C�rdenas Perales
	 */

	static Scanner input = new Scanner(System.in);
	static GestorCliente gestor = new GestorCliente();

	public static void main(String[] args) {
		altaClienteSesion();
		menuPrincipal();
	}

	public static void menuPrincipal() {
		gestor.backupSesiones();
		int opcion;
		do {
			System.out.println("***************************************");
			System.out.println("Bienvenido al men� de gesti�n de fisio");
			System.out.println("1.- Gesti�n cliente");
			System.out.println("2.- Gesti�n sesi�n");
			System.out.println("3.- Salir");
			System.out.println("***************************************");
			opcion = input.nextInt();
			switch (opcion) {
			case 1:
				enunciadoCliente();
				break;
			case 2:
				enunciadoSesion();
				break;
			case 3:
				System.out.println("Salir!, gracias por usarme");
				break;
			default:
				System.out.println("Opci�n no valida");
				break;
			}
		} while (opcion != 3);

	}

	public static void enunciadoSesion() {
		int opcion;
		do {
			System.out.println("***************************************************");
			System.out.println("1.- Alta sesi�n fichero:");
			System.out.println("2.- Visualizar sesiones fichero:");
			System.out.println("3.- Modificar nombre sesi�n:");
			System.out.println("4.- Mostrar nombre de las sesiones");
			System.out.println("5.- Mostrar n�mero de sesiones por estado");
			System.out.println("6.- Tama�o del fichero en bytes");
			System.out.println("7.- Cambiar valor del precio");
			System.out.println("8.- Alta material en sesiones");
			System.out.println("9.- Salir");
			System.out.println("Por favor eliga una opci�n");
			System.out.println("***************************************");
			opcion = input.nextInt();
			menuSesion(opcion);
		} while (opcion != 9);
	}

	public static void enunciadoCliente() {
		int opcion;
		do {
			System.out.println("***************************************************");
			System.out.println("1.- Alta de un cliente:");
			System.out.println("2.- Cargando los datos cliente:");
			System.out.println("3.- Visualizar los datos cliente:");
			System.out.println("4.- Buscar cliente:");
			System.out.println("5.- Cliente con m�s sesiones");
			System.out.println("6.- Nombre de los clientes en May�sculas");
			System.out.println("7.- Nombre y contrase�a de los clientes");
			System.out.println("8.- Total de sesiones");
			System.out.println("9.- Observaciones");
			System.out.println("10.- Salir");
			System.out.println("Por favor eliga una opci�n");
			System.out.println("***************************************");
			opcion = input.nextInt();
			menuCliente(opcion);
		} while (opcion != 10);
	}

	public static void menuCliente(int opcion) {
		switch (opcion) {
		case 1:
			gestor.guardarDatos();
			break;
		case 2:
			gestor.cargarDatos();
			break;
		case 3:
			gestor.visualizarDatos();
			break;
		case 4:
			gestor.buscarCliente();
			break;
		case 5:
			gestor.visualizarClienteMasSesiones();
			break;
		case 6:
			gestor.cambiarNombreMayus();
			break;
		case 7:
			gestor.nombreContrasenaCliente();
			break;
		case 8:
			gestor.totalSesiones();
			break;
		case 9:
			gestor.observacionesTecnicos();
			break;
		case 10:
			System.out.println("Salir!");
			break;
		default:
			System.out.println("No existe la opci�n");
			break;
		}
	}

	public static void menuSesion(int opcion) {
		switch (opcion) {
		case 1:
			gestor.altaSesionFichero();
			break;
		case 2:
			gestor.visualizarSesionesArchivo();
			break;
		case 3:
			gestor.modificarSesion();
			break;
		case 4:
			gestor.mostrarNombres();
			break;
		case 5:
			gestor.numeroEstadoSesiones();
			break;
		case 6:
			gestor.longitudDelFichero();
			break;
		case 7:
			gestor.cambioValorPrecio();
			break;
		case 8:
			gestor.altaMaterialesSesion();
			break;
		case 9:
			System.out.println("Salir!");
			break;
		default:
			System.out.println("No existe la opci�n");
			break;
		}
	}

	public static void altaClienteSesion() {
		gestor.altaCliente("Daniel", "daniel@daniel.com", "1998-06-09", "Daniel", "1234", 1);
		gestor.altaCliente("Marcos", "Marcos@Marcos.com", "2000-07-07", "Marcos", "12345", 12);
		gestor.altaCliente("Miguel", "Miguel@Miguel.com", "1990-12-12", "Miguel", "123456", 23);
		gestor.altaCliente("Antonio", "Antonio@Antonio.com", "2002-08-23", "Antonio", "1234567", 11);
		gestor.altaCliente("Maria", "Maria@Maria.com", "1995-11-23", "Maria", "12345678", 34);
		gestor.altaCliente("Adrian", "Adrian@Adrian.com", "1997-07-09", "Adrian", "123456789", 44);
		gestor.altaCliente("Jorge", "Jorge@Jorge.com", "2000-07-12", "Jorge", "12345678910", 12);
		gestor.altaCliente("Sancho", "Sancho@Sancho.com", "1990-12-12", "Sancho", "123454", 21);
		gestor.altaCliente("Macarena", "Macarena@Macarena.com", "1999-12-23", "Macarena", "12321", 24);
		gestor.altaCliente("Nerea", "Nerea@Nerea.com", "1998-02-12", "Nerea", "987234", 9);
		gestor.altaSesion("sesion1", "confirmado", 34.34f, true);
		gestor.altaSesion("sesion2", "confirmado", 45.10f, false);
		gestor.altaSesion("sesion3", "cancelado", 23.20f, true);
		gestor.altaSesion("sesion4", "cancelado", 76.44f, true);
		gestor.altaSesion("sesion5", "desconocido", 78.56f, false);
		gestor.altaSesion("sesion6", "error", 23.34f, true);
		gestor.altaSesion("sesion7", "error", 56.54f, false);
		gestor.altaSesion("sesion8", "confirmado", 78.56f, false);
		gestor.altaSesion("sesion9", "confirmado", 23.34f, true);
		gestor.altaSesion("sesion10", "error", 56.54f, false);
	}
}

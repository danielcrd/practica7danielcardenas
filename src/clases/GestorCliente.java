package clases;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

public class GestorCliente {
	public Scanner input = new Scanner(System.in);

	/**
	 * @author Daniel C�rdenas Perales
	 */

	/**
	 * Creamos el ArrayList de la clase cliente . Cliente -> listadoClientes Creamos
	 * el ArrayList de la clase sesion . Sesion -> listadoSesiones
	 */
	private ArrayList<Cliente> listadoClientes;
	private ArrayList<Sesion> listadoSesiones;

	/**
	 * Creamos el constructor de GestorCliente, inicializando el arraylist cliente y
	 * sesi�n.
	 */
	public GestorCliente() {
		this.listadoClientes = new ArrayList<Cliente>();
		this.listadoSesiones = new ArrayList<Sesion>();
	}

	/******************************************************************************************************/
	// CLIENTE

	/**
	 * Metodo el cual su funci�n es dar de alta un cliente, comprobaremos si el
	 * cliente existe, si el cliente existe, nos mostrara un mensaje diciendo que el
	 * cliente ya existe
	 * 
	 * @param nombre          nombre del cliente
	 * @param email           email del cliente
	 * @param fechaNacimiento fecha de nacimiento del cliente
	 * @param usuario         usuario del cliente
	 * @param contrasena      constrase�a del cliente
	 * @param numeroSesiones  numero de sesiones del cliente
	 * @return void
	 */

	public void altaCliente(String nombre, String email, String fechaNacimiento, String usuario, String contrasena,
			int numeroSesiones) {
		if (!existeCliente(usuario)) {
			Cliente clienteNuevo = new Cliente(nombre, usuario, contrasena);
			clienteNuevo.setEmail(email);
			clienteNuevo.setFechaNacimiento(LocalDate.parse(fechaNacimiento));
			clienteNuevo.setActivo(true);
			clienteNuevo.setNumeroSesiones(numeroSesiones);
			listadoClientes.add(clienteNuevo);
		} else {
			System.out.println("Ese cliente ya existe");
		}
	}

	/**
	 * metodo que comprueba si existe un cliente o no, mediante un if comprobando si
	 * el cliente es lo contrario de nulo y tambien comprobando el usuario que tiene
	 * asignado el cliente con el usuario escrito en el menu al llamar al metodo
	 * 
	 * @param usuario usuario del cliente
	 * @return boolean para comfirmar la existencia del cliente
	 */

	public boolean existeCliente(String usuario) {
		for (Cliente cliente : listadoClientes) {
			if (cliente != null && cliente.getUsuario().equals(usuario)) {
				return true;
			}
		}
		return false;
	}

	/******************************************************************************************************/
	// SESI�N

	/**
	 * metodo que su funci�n es dar de alta sesiones en un arraylist
	 * 
	 * @param nombre nombre de la sesi�n
	 * @param estado estado de la sesi�n
	 * @param precio precio de la sesi�n
	 * @param cupon  comprobar si tiene un cup�n asignado
	 */
	public void altaSesion(String nombre, String estado, float precio, boolean cupon) {
		listadoSesiones.add(new Sesion(nombre, estado, precio, cupon));
	}

	/**
	 * metodo que su funci�n es listar cada una de las sesiones que han sido dadas
	 * de alta en el metodo de altaSesion
	 * 
	 */
	public void listarSesiones() {
		for (Sesion sesion : listadoSesiones) {
			System.out.println(sesion);
		}
	}

	/******************************************************************************************************/
	// FICHEROS SECUENCIALES

	/**
	 * Metodo que su funcion es guardar los datos de clientes en un fichero
	 */
	public void guardarDatos() {
		try {
			File fichero = new File("src/ficheros/datosCliente.txt");
			if (!fichero.exists()) {
				fichero.createNewFile();
			}
			ObjectOutputStream escritura = new ObjectOutputStream(
					new FileOutputStream(new File("src/ficheros/datosCliente.txt")));
			datosCliente();
			escritura.writeObject(listadoClientes);
			System.out.println("Los datos han sido guardados");
			escritura.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Met�do que su funci�n es introducir los datos para dar de alta un cliente
	 */
	public void datosCliente() {
		System.out.println("Por favor introduzca el nombre del cliente");
		String nombre = input.nextLine();
		System.out.println("Por favor introduzca el email del cliente");
		String email = input.nextLine();
		System.out.println("Por favor introduzca la fecha de nacimiento del cliente");
		String fechaNacimiento = input.nextLine();
		System.out.println("Por favor introduzca el usuario del cliente");
		String usuario = input.nextLine();
		System.out.println("Por favor introduzca la contrase�a del cliente");
		String contrasena = input.nextLine();
		System.out.println("Por favor introduzca el n�mero de las sesiones que ha realizado el cliente");
		int numeroSesiones = input.nextInt();
		input.nextLine();
		altaCliente(nombre, email, fechaNacimiento, usuario, contrasena, numeroSesiones);
	}

	/**
	 * Metodo que su funcion es cargar los datos de un fichero al arrayList de un
	 * cliente
	 */
	@SuppressWarnings("unchecked")
	public void cargarDatos() {
		try {
			ObjectInputStream escritura = new ObjectInputStream(
					new FileInputStream(new File("src/ficheros/datosCliente.txt")));
			System.out.println("Borramos todos los datos del listado de clientes: ");
			listadoClientes.clear();
			System.out.println(listadoClientes);
			listadoClientes = (ArrayList<Cliente>) escritura.readObject();
			System.out.println("Los datos han sido cargados");
			escritura.close();
		} catch (EOFException e) {
			System.out.println("No hay datos en el fichero para poder cargar");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Metodo que su funcion es visualizar los datos de un fichero, este fichero
	 * contendra un conjunto de clientes del centro de fisio
	 */
	@SuppressWarnings("unchecked")
	public void visualizarDatos() {
		ObjectInputStream escritura;
		try {
			escritura = new ObjectInputStream(new FileInputStream(new File("src/ficheros/datosCliente.txt")));
			System.out.println((ArrayList<Cliente>) escritura.readObject());
		} catch (IOException | ClassNotFoundException e) {
			// TODO Auto-generated catch block
			System.out.println("No hay datos en el fichero");
		}
	}

	/**
	 * Metodo que su funcion es buscar en los datos de un fichero el cliente
	 * asociado mediante su nombre, si el cliente no es encontrado, nos mostrara un
	 * mensaje indicandolo
	 */
	@SuppressWarnings("unchecked")
	public void buscarCliente() {
		String nombre;

		System.out.println("Introduce el cliente a buscar");
		nombre = input.nextLine();
		// declaramos el arrayList de Cliente
		ArrayList<Cliente> buscar = new ArrayList<Cliente>();

		try {
			// 1.- abro para lectura
			ObjectInputStream escritura = new ObjectInputStream(
					new FileInputStream(new File("src/ficheros/datosCliente.txt")));
			// 2.- asignamos "buscar" el valor de la escritura de todos los clientes que
			// estaban guardados en el fichero
			buscar = (ArrayList<Cliente>) escritura.readObject();
			int contador = 0;
			// 3.- buscamos la palabra en el vector
			for (Cliente cliente : buscar) {
				if (cliente.getNombre().equals(nombre)) {
					// 4.- Mostramos el cliente y confirmamos que existe mediante el contador
					System.out.println(cliente);
					contador++;
				}
			}
			// Si no lo encuentra nos mostrara un error
			if (contador == 0) {
				System.out.println("El cliente no ha sido encontrado");
			}

			// 4.- cerramos el archivo
			escritura.close();
		} catch (FileNotFoundException e) {
			System.out.println("Fichero no encontrado");
		} catch (IOException e) {
			System.out.println("No hay datos en el fichero");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Metodo que su funcion es encontrar en los datos de un fichero el cliente con
	 * mas sesiones, cuando lo encuentre, nos creara un fichero solo para guardar el
	 * nombre del cliente y el n�mero de sesiones que ha realizado.
	 */

	// Acci�n extra n�: operaci�n realizada
	@SuppressWarnings("unchecked")
	public void visualizarClienteMasSesiones() {
		try {
			// 1.- declaramos el arrayList llamado clienteSesion
			ArrayList<Cliente> clienteSesion = new ArrayList<Cliente>();
			// 2.- abro para la lectura del fichero
			ObjectInputStream lectura = new ObjectInputStream(
					new FileInputStream(new File("src/ficheros/datosCliente.txt")));
			// 3.- sacamos todos los clientes en un arraylist y asignamos el primer cliente
			// al objeto
			clienteSesion = (ArrayList<Cliente>) lectura.readObject();
			Cliente clienteMasSesiones = clienteSesion.get(0);

			// 4.- buscamos el cliente con mas sesiones en el arrayList
			for (int i = 0; i < clienteSesion.size(); i++) {
				if (clienteMasSesiones.getNumeroSesiones() < clienteSesion.get(i).getNumeroSesiones()) {
					clienteMasSesiones = clienteSesion.get(i);
				}
			}

			// 5.- Almacenamos el cliente en el archivo especifico
			File fichero = new File("src/ficheros/datosClienteSesion.txt");
			if (!fichero.exists()) {
				fichero.createNewFile();
			}
			ObjectOutputStream escritura = new ObjectOutputStream(
					new FileOutputStream(new File("src/ficheros/datosClienteSesion.txt")));
			ObjectInputStream lecturaClienteSesion = new ObjectInputStream(
					new FileInputStream(new File("src/ficheros/datosClienteSesion.txt")));
			escritura.writeObject("El cliente: " + clienteMasSesiones.getNombre() + " tiene un total de sesiones: "
					+ clienteMasSesiones.getNumeroSesiones());

			// 6.- Mostramos el cliente con mas sesiones del fichero
			System.out.println(lecturaClienteSesion.readObject());

			// 7.- Cerramos los archivos
			lecturaClienteSesion.close();
			lectura.close();
			escritura.close();
		} catch (EOFException e) {
			System.out.println("El fichero: datosCliente, no tiene datos");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Metodo que su funciones es cambiar los nombres de cliente a mayuscula, para
	 * ello, cargaremos los datos de un fichero, recorreremos todos los clientes
	 * para cambiar el nombre en formato Mayuscula y despues lo mostraremos
	 */

	// Acci�n extra n�: operaci�n realizada
	public void cambiarNombreMayus() {

		try {
			// 1.- Cambiamos los nombres a mayusculas y lo almacenamos en el fichero
			ObjectOutputStream escritura = new ObjectOutputStream(
					new FileOutputStream(new File("src/ficheros/nombreClientes.txt")));

			for (Cliente cliente : listadoClientes) {
				String nombre = cliente.getNombre().toUpperCase();
				escritura.writeObject(nombre);
			}

			// 2.- Visualizamos los datos del fichero
			ObjectInputStream lecturaNombre = new ObjectInputStream(
					new FileInputStream(new File("src/ficheros/nombreClientes.txt")));
			for (int i = 0; i < listadoClientes.size(); i++) {
				System.out.println(lecturaNombre.readObject());
			}

			// 3.- Cerramos los ficheros
			escritura.close();
			lecturaNombre.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// numero total de sesiones
	/**
	 * Metodo cuya funci�n es sumar todas las sesiones que han realizado todos los
	 * clientes, para poder sacar el listado de los clientes tendremos que
	 * actualizar el arraylist mediante, el metodo cargar datos, despues a�adiremos
	 * la cantidad en un fichero de texto
	 */

	// Acci�n extra n�: operaci�n realizada
	public void totalSesiones() {
		// 1.- recorremos el arrayList y utilizamos un parametro int para ir sumandolas
		// de uno en uno
		int suma = 0;
		for (Cliente cliente : listadoClientes) {
			suma += cliente.getNumeroSesiones();
		}

		// 2.- Creamos un fichero y almacenamos los datos
		ObjectOutputStream escritura;
		try {
			escritura = new ObjectOutputStream(new FileOutputStream(new File("src/ficheros/totalSesiones.txt")));
			escritura.writeObject("N�mero total de sesiones: " + suma);

			// 3.- Visualizamos el fichero
			ObjectInputStream lectura = new ObjectInputStream(
					new FileInputStream(new File("src/ficheros/totalSesiones.txt")));
			System.out.println(lectura.readObject());
			// 4.- Cerramos los ficheros
			escritura.close();
			lectura.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * metodo que su funci�n es a�adir unas observaciones que realiza los empleados
	 * del centro, donde podran a�adir comentarios al respeto sobre sus necesidades
	 * y recetas para los clientes
	 */

	// Acci�n extra n�: operaci�n realizada
	public void observacionesTecnicos() {
		// 1.- abro para la entrada de teclado
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		String linea;
		// 2.- abro para escritura con la clase PrintWriter
		PrintWriter fuente;
		try {
			fuente = new PrintWriter(new BufferedWriter(new FileWriter("src/ficheros/observaciones.txt", true)));
			System.out.println("Creaci�n de un archivo \n" + "Introducir lineas (* para acabar)");
			// 3.- recorro linea a linea hasta *
			linea = in.readLine();
			while (!linea.equalsIgnoreCase("*")) {
				fuente.println(linea);
				linea = in.readLine();
			}
			// 4.- cerrar el archivo
			fuente.close();

			// 1.- abro para lectura del archivo
			BufferedReader lectura = new BufferedReader(new FileReader("src/ficheros/observaciones.txt"));
			// 2.- recorro linea a linea hasta null
			linea = lectura.readLine();
			while (linea != null) {
				System.out.println(linea);
				linea = lectura.readLine();
			}
			// 3.- cierro el archivo
			lectura.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * metodo cuya funcion es almacenar los nombres y las contrase�as de los
	 * clientes del centro de fisio, el fichero se llamara: contrasenaClientes.txt
	 */

	// Acci�n extra n�: operaci�n realizada
	public void nombreContrasenaCliente() {

		try {
			// 1.- A�adimos el nombre y la contrase�a de los clientes al fichero
			ObjectOutputStream escritura = new ObjectOutputStream(
					new FileOutputStream(new File("src/ficheros/contrasenaClientes.txt")));

			for (Cliente cliente : listadoClientes) {
				escritura.writeObject("Nombre: " + cliente.getNombre() + ",Contrase�a: " + cliente.getContrasena());
			}

			// 2.- Visualizamos los datos del fichero
			ObjectInputStream lecturaNombre = new ObjectInputStream(
					new FileInputStream(new File("src/ficheros/contrasenaClientes.txt")));
			for (int i = 0; i < listadoClientes.size(); i++) {
				System.out.println(lecturaNombre.readObject());
			}

			// 3.- Cerramos los ficheros
			escritura.close();
			lecturaNombre.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/******************************************************************************************************/
	// RAF

	/**
	 * metodo que su funci�n es a�adir sesiones a un fichero aleatorio llamado:
	 * listaSesiones.txt, datos que tendra que introducir, nombre, estado, precio,
	 * cupon
	 */
	public void altaSesionFichero() {
		try {
			// 1.- Abrimos el archivo: listaSesiones.txt y a�adimos los datos
			RandomAccessFile f = new RandomAccessFile("src/ficheros/listaSesiones.txt", "rw");
			// 2.- Nos colocamos en la �ltima fila para no sobrescribir
			f.seek(f.length());
			String respuesta = "";
			do {
				// 3.- Introducimos los datos
				System.out.println("Por favor introduzca el nombre de la sesion ");
				String nombre = input.nextLine();
				nombre = formatearNombre(nombre, 20);
				f.writeUTF(nombre);
				System.out.println("Por favor introduzca el estado de la sesion ");
				System.out.println("error -- confirmado -- cancelado -- desconocido");
				String estado = input.nextLine();
				estado = formatearNombre(estado, 21);
				f.writeUTF(estado);
				System.out.println("Por favor introduzca el precio de la sesion ");
				String precio = input.nextLine();
				precio = formatearNombre(precio, 22);
				f.writeUTF(precio);
				System.out.println("�La sesion tiene cup�n? ");
				String cupon = input.nextLine();
				precio = formatearNombre(cupon, 5);
				f.writeUTF(cupon);
				System.out.println("�Deseas continuar (si/no)?");
				respuesta = input.nextLine();
			} while (respuesta.equalsIgnoreCase("si"));
			// 4.- cierro el archivo
			f.close();
			f.close();
		} catch (IOException e) {
			System.out.println("error");
		}
	}

	/**
	 * metodo que su funci�n es a�adir a un parametro introducido de tipo String una
	 * cantidad fija para luego poder usarla para diferentes operaciones
	 * 
	 * @param nombre nombre del parametro para a�adir x bytes
	 * @param lon    el tama�o a a�adir
	 * @return String el parametro formateado
	 */
	public String formatearNombre(String nombre, int lon) {
		// convertir la cadena al tama�o indicado
		if (nombre.length() > lon) {
			// recorta
			return nombre.substring(0, lon);
		} else {
			// completar con espacios en blanco
			for (int i = nombre.length(); i < lon; i++) {
				nombre = nombre + " ";
			}
		}
		return nombre;
	}

	/**
	 * Metodo que su funci�n es agregar diferentes sesiones que estan guardadas en
	 * el arraylist en el fichero aleatorio llamado: listaSesiones.txt
	 */
	// Acci�n extra n�: operaci�n realizada
	public void backupSesiones() {
		try {

			// 1.- abro el archivo para escribir
			RandomAccessFile escritura = new RandomAccessFile("src/ficheros/listaSesiones.txt", "rw");
			if (escritura.length() == 0) {
				// 2.- leo vector y escribo en el archivo abierto anteriormente
				for (Sesion sesion : listadoSesiones) {
					escritura.seek(escritura.length());
					String nombre = sesion.getNombre();
					nombre = formatearNombre(nombre, 20);
					escritura.writeUTF(nombre);
					String estado = sesion.getEstado();
					estado = formatearNombre(estado, 21);
					escritura.writeUTF(estado);
					String precio = String.valueOf(sesion.getPrecio());
					precio = formatearNombre(precio, 22);
					escritura.writeUTF(precio);
					String cupon = String.valueOf(sesion.isCupon());
					cupon = formatearNombre(cupon, 5);
					escritura.writeUTF(cupon);
				}
				// 3.- cierro el archivo
				escritura.close();
			}
		} catch (IOException e) {
			System.out.println("Error de entrada salida");
			System.exit(0);
		}
	}

	/**
	 * metodo que su funci�n es mostrar todas las sesiones que se encuentran en el
	 * fichero: listaSesiones.txt, al finalizar el fichero nos mostrara un mensaje
	 * comentado que ya se ha acabado el fichero
	 */
	public void visualizarSesionesArchivo() {
		try {
			// 1.- abro el archivo para poder visualizarlo
			RandomAccessFile f = new RandomAccessFile("src/ficheros/listaSesiones.txt", "rw");
			// 2.- creo los parametros de cada una de las caracteristicas de una sesi�n
			String nombre = "";
			String estado = "";
			String precio = "";
			String cupon = "";
			boolean finFichero = false;
			do {
				try {
					nombre = f.readUTF();
					System.out.println("Nombre: " + nombre);
					estado = f.readUTF();
					System.out.println("Estado: " + estado);
					precio = f.readUTF();
					System.out.println("Precio: " + precio);
					cupon = f.readUTF();
					System.out.println("Cupon: " + cupon);
					System.out.println("*********************************************");
				} catch (EOFException e) {
					System.out.println("Fin fichero");
					finFichero = true;
					// 3.- Cierro el fichero
					f.close();
				}

			} while (!finFichero);
		} catch (IOException e) {
			System.out.println("error");
		}
	}

	/**
	 * metodo que su funciones es cambiar el nombre de la sesi�n por uno nuevo
	 * introducido en el propio metodo si no encuentra dicha sesi�n, nos visualizara
	 * un mensaje indicandolo
	 */
	public void modificarSesion() {
		try {
			// 1.- pido el nombre del valor para poder relacionarlo con la sesi�n
			BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
			System.out.println("Por favor introduzca el nombre que quieres cambiar");
			String nombreAnterior = input.readLine();
			// 2.- pido el nombre nuevo para asignarlo a la sesi�n
			System.out.println("Por favor introduzca el nuevo nombre");
			String nombreNuevo = input.readLine();
			// 3.- abro el archivo
			RandomAccessFile f = new RandomAccessFile("src/ficheros/listaSesiones.txt", "rw");
			// 4.- busco en el fichero el nombre y cuando lo encuentre cambio el valor
			String nombre;
			boolean finFichero = false;
			boolean valorEncontrado = false;
			do {
				try {
					nombre = f.readUTF();
					if (nombre.trim().equalsIgnoreCase(nombreAnterior)) {
						f.seek(f.getFilePointer() - 22);
						nombreNuevo = formatearNombre(nombreNuevo, 20);
						f.writeUTF(nombreNuevo);
						valorEncontrado = true;
					}
				} catch (EOFException e) {
					f.close();
					finFichero = true;
				}
			} while (!finFichero);
			if (valorEncontrado == false) {
				System.out.println("La sesi�n no existe");
			} else {
				System.out.println("La sesi�n ha sido modificada");
			}
		} catch (IOException e) {
			System.out.println("error");
		}
	}

	/**
	 * metodo que su funcion es mostrar los nombres de las sesiones, para ello
	 * pediremos una contrase�a, si la contrase�a es correcta nos mostrara los
	 * nombres de las sesiones, si no, nos dara un mensaje
	 */
	// Acci�n extra n�: operaci�n realizada
	public void mostrarNombres() {
		System.out.println("Por favor introduzca la contrase�a");
		String contrasena = input.nextLine();
		System.out.println("Por favor introduzca la contrase�a de nuevo");
		String contrasenaNuevo = input.nextLine();
		if (contrasena.equals("1234") && contrasenaNuevo.equals("1234")) {
			try {
				// abrimos el archivo listaSesiones
				RandomAccessFile f = new RandomAccessFile("src/ficheros/listaSesiones.txt", "rw");
				String nombre;
				boolean finFichero = false;
				System.out.println("Nombre de las sesiones");
				do {
					try {
						nombre = f.readUTF();
						if (nombre.length() == 20) {
							System.out.println(nombre);
						}
					} catch (EOFException e) {
						try {
							f.close();
						} catch (IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						finFichero = true;
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} while (!finFichero);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			System.out.println("Contrase�a incorrecta");
		}
	}

	/**
	 * metodo que su funcion es contar cuantas sesiones estan en cada uno de los
	 * diferentes estados
	 */
	// Acci�n extra n�: operaci�n realizada
	public void numeroEstadoSesiones() {
		try {
			RandomAccessFile f = new RandomAccessFile("src/ficheros/listaSesiones.txt", "rw");
			String linea;
			boolean finFichero = false;
			int contadorConfirmado = 0;
			int contadorError = 0;
			int contadorDesconocido = 0;
			int contadorCancelado = 0;
			do {
				try {
					linea = f.readUTF();
					if (linea.trim().equals("confirmado")) {
						contadorConfirmado++;
					} else if (linea.trim().equals("error")) {
						contadorError++;
					} else if (linea.trim().equals("desconocido")) {
						contadorDesconocido++;
					} else if (linea.trim().equals("cancelado")) {
						contadorCancelado++;
					}
				} catch (EOFException e) {
					try {
						f.close();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					finFichero = true;
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} while (!finFichero);
			System.out.println("Sesiones con estado confirmado: " + contadorConfirmado);
			System.out.println("Sesiones con estado error: " + contadorError);
			System.out.println("Sesiones con estado desconocido: " + contadorDesconocido);
			System.out.println("Sesiones con estado cancelado: " + contadorCancelado);

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/*
	 * metodo que sun funci�n es mostrar la longitud del fichero en bytes
	 */
	// Acci�n extra n�: operaci�n realizada
	public void longitudDelFichero() {
		try {
			RandomAccessFile longitud = new RandomAccessFile("src/ficheros/listaSesiones.txt", "rw");
			System.out.println("El tama�o del fichero en bytes es de: " + longitud.length());
			longitud.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * metodo que su funci�n es a�adir o bajar el precio de todas las sesiones
	 */
	// Acci�n extra n�: operaci�n realizada
	public void cambioValorPrecio() {
		float numeros = precio();
		input.nextLine();
		try {
			RandomAccessFile f = new RandomAccessFile("src/ficheros/listaSesiones.txt", "rw");
			String precio;
			boolean finFichero = false;
			do {
				try {
					precio = f.readUTF();
					if (precio.length() == 22) {
						f.seek(f.getFilePointer() - 24);
						float precioCambiar = Float.parseFloat(precio);
						precioCambiar = precioCambiar + numeros;
						String precioNuevo = String.valueOf(precioCambiar);
						precioNuevo = formatearNombre(precioNuevo, 22);
						f.writeUTF(precioNuevo);
					}
				} catch (EOFException e) {
					try {
						f.close();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					finFichero = true;
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} while (!finFichero);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Metodo que su funci�n es dar de alta en un fichero materiales que se van a
	 * usar en una sesi�n
	 */
	// Acci�n extra n�: operaci�n realizada
	public void altaMaterialesSesion() {
		RandomAccessFile f;
		try {
			f = new RandomAccessFile("src/ficheros/listaMaterialesSesion.txt", "rw");
			f.seek(f.length());
			String respuesta = "";
			do {
				System.out.println("Por favor introduzca el nombre de la sesi�n: ");
				String nombre = input.nextLine();
				nombre = formatearNombre(nombre, 20);
				f.writeUTF(nombre);
				System.out.println("Por favor introduzca el material de la sesi�n: ");
				String material = input.nextLine();
				material = formatearNombre(material, 50);
				f.writeUTF(material);
				System.out.println("�Deseas continuar (si/no)?");
				respuesta = input.nextLine();
			} while (respuesta.equalsIgnoreCase("si"));
			visualizarSesionesMateriales();
			f.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Metodo que su funci�n es mostrar de un fichero, materiales que se van a usar
	 * en las sesiones
	 */
	// Acci�n extra n�: operaci�n realizada
	public void visualizarSesionesMateriales() {
		try {
			RandomAccessFile f = new RandomAccessFile("src/ficheros/listaMaterialesSesion.txt", "rw");
			String nombre = "";
			String materiales = "";
			boolean finFichero = false;
			do {
				try {
					nombre = f.readUTF();
					System.out.println("Nombre de la sesi�n: " + nombre);
					System.out.println("Materiales de la sesi�n");
					materiales = f.readUTF();
					System.out.println(materiales);
					System.out.println("*********************************************");
				} catch (EOFException e) {
					System.out.println("Fin fichero");
					finFichero = true;
					f.close();
				}

			} while (!finFichero);
		} catch (IOException e) {
			System.out.println("error");
		}
	}

	/**
	 * metodo que su funcion es comprobar que se escribe correctamente el precio de
	 * una sesi�n, solo pueden ser n�meros decimales, si no es asi, nos mostrara el
	 * mensaje correspondiente
	 * 
	 * @return precio de la sesi�n
	 */
	public float precio() {
		boolean comprobacion = false;
		float precioNuevo = 0;
		do {
			try {
				System.out.println("Por favor introduzca el precio de la sesi�n: ");
				precioNuevo = input.nextFloat();
				comprobacion = false;
			} catch (InputMismatchException e) {
				System.out.println("Caracter introducido no valido");
				comprobacion = true;
				input.nextLine();
			} catch (Exception e) {
				System.out.println("Algo no funciona bien");
				comprobacion = true;
				input.nextLine();
			}
		} while (comprobacion);
		comprobacion = false;
		return precioNuevo;
	}
}

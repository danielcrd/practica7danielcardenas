package clases;

public class Sesion {

	/**
	 * @author Daniel C�rdenas Perales
	 */

	/**
	 * Atributos de la sesi�n, todos con el modificador de visibilidad private, para
	 * sacar o cambiar los valores se necesitara utilizar los metodos getter and
	 * setter.
	 */

	protected String nombre;
	protected String estado;
	protected float precio;
	protected boolean cupon;

	/**
	 * Constructor de la Sesion, se debe introducir el nombre y el estado para poder
	 * crear un objeto
	 * 
	 * @param nombre nombre de la sesi�n
	 * @param estado estado de la sesi�n
	 */

	public Sesion(String nombre, String estado) {
		this.nombre = nombre;
		this.estado = estado;
	}

	/**
	 * contrsuctor de la sesi�n, en este se tendra que introducir todos los
	 * atributos para crearlo.
	 * 
	 * @param nombre      nombre de la sesi�n
	 * @param estado      estado de la sesi�n
	 * @param precio      precio de la sesi�n
	 * @param nombreFisio nombre del Fisio que realizara la sesi�n
	 * @param cupon       booleano para comprobar si la sesion tiene un cupon o no
	 * 
	 */

	public Sesion(String nombre, String estado, float precio, boolean cupon) {
		this.nombre = nombre;
		this.estado = estado;
		this.precio = precio;
		this.cupon = cupon;
	}

	/**
	 * Metodo que nos devuelve el nombre de la sesi�n
	 * 
	 * @return un String con el nombre
	 */

	public String getNombre() {
		return nombre;
	}

	/**
	 * metodo para cambiar el nombre de la sesi�n
	 * 
	 * @param nombre nombre de la sesi�n
	 */

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * metodo el cual nos devuelve el estado de una sesi�n
	 * 
	 * @return un String con el estado de la sesi�n
	 */

	public String getEstado() {
		return estado;
	}

	/**
	 * metodo para cambiar el estado de una sesi�n
	 * 
	 * @param estado el estado de la sesi�n
	 */

	public void setEstado(String estado) {
		this.estado = estado;
	}

	/**
	 * metodo que devuelve el precio de una sesi�n
	 * 
	 * @return un int con el valor de la sesi�n
	 */

	public float getPrecio() {
		return precio;
	}

	/**
	 * metodo que cambia el valor de una sesi�n
	 * 
	 * @param precio precio de la sesi�n llamada
	 */

	public void setPrecio(float precio) {
		this.precio = precio;
	}

	/**
	 * metodo que devuelve un booleano diciendo si tiene un cup�n la sesi�n o no
	 * 
	 * @return un booleano con el valor de true o false dependiendo de la sesi�n
	 */

	public boolean isCupon() {
		return cupon;
	}

	/**
	 * metodo para cambiar el valor del atributo cupon, para comprobar si tiene o no
	 * un cup�n la sesi�n
	 * 
	 * @param cupon valor para diferenciar si tiene un cup�n o no
	 */

	public void setCupon(boolean cupon) {
		this.cupon = cupon;
	}

	/**
	 * metodo el cual devolvera todos los valores de cada uno de los atributos
	 * asignados
	 */

	@Override
	public String toString() {
		return "Sesion [nombre=" + nombre + ", estado=" + estado + ", precio=" + precio + ", cupon=" + cupon + "]";
	}

}
